package com.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class searchBar {

    private WebDriver driver;

    @FindBy(xpath = "//input[@id='twotabsearchtextbox']")
    private WebElement searchBar;

    @FindBy(xpath = "//input[@id='nav-search-submit-button']")
    private WebElement searchButton;

    public searchBar(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver , this);
    }

    public  boolean search(String product){
         try {
             visiblity(searchBar);
             searchBar.sendKeys(product);
             searchButton.click();
             return  true;
         }catch (Exception e){
             return false;
         }
    }

    public  void visiblity(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver , Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(element));
    }
}
