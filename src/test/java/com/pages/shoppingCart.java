package com.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class shoppingCart {
    private WebDriver driver;

//    @FindBy(xpath = "//input[@id='twotabsearchtextbox']")
//    private WebElement searchBar;

//    @FindBy(xpath = "//input[@id='nav-search-submit-button']")
//    private WebElement searchButton;


    @FindBy(xpath = "//button[@class='a-button-text']")
    private  WebElement addToCart;

    @FindBy (xpath = "//input[@name='proceedToRetailCheckout']")
    private  WebElement proceedTObuy;

    @FindBy(css = "#nav-cart.nav-a.nav-a-2")
    private WebElement cartButton;

    public shoppingCart(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver , this);
    }
    public boolean cart(String product){
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("window.scrollBy(0, 500);");
            visiblity(addToCart);
            addToCart.click();
            proceedTObuy.click();


            return true;
        }catch (Exception e){
            return  false;
        }
    }

    public void visiblity(WebElement element){
        WebDriverWait waits = new WebDriverWait(driver , Duration.ofSeconds(10));
        waits.until(ExpectedConditions.visibilityOf(element));
    }
}
