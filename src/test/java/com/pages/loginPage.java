package com.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class loginPage {


    private WebDriver driver;

    @FindBy(xpath = "//span[@class ='nav-line-2 ']")
    private WebElement Account;

    @FindBy(id = "nav-link-accountList-nav-line-1")
    private  WebElement signInButton;

    @FindBy(css = "input#ap_email")
    private  WebElement emailOrPhone;

    @FindBy(xpath = "//span[@id='continue']")
    private WebElement countinueButoon;

    @FindBy(css = "input#ap_password")
    private  WebElement password;

    @FindBy(xpath = "//span[@id='auth-signin-button']")
    private  WebElement Button;

    public   loginPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver , this);
    }

    public  boolean signIn(String email , String Pass){
        try {
            Account.click();
            visiblity(emailOrPhone);
            emailOrPhone.sendKeys(email);
            countinueButoon.click();
            visiblity(password);
            password.sendKeys(Pass);
            Button.click();

            return true;
        }catch (Exception e){
            return false;
        }

    }

    private void visiblity(WebElement element){

        WebDriverWait wait = new WebDriverWait(driver , Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(element));
    }
}
