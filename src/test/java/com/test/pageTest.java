package com.test;

import com.pages.loginPage;
import com.pages.searchBar;
import com.pages.shoppingCart;
import com.pages.signOut;
import org.example.configReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class pageTest {

    public WebDriver driver;
    private configReader configration;

    @BeforeTest
    public void setUp(){
        configration = new configReader();
        driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver","/home/vishal/Downloads/chromedriver");
        driver.manage().window().maximize();
    }

    @Test(priority = 1)
    public void testHomepage() throws InterruptedException {
        driver.get("https://www.amazon.com/");
        String ExpectedOutput = "Amazon.com. Spend less. Smile more.";
        String actualOutput = driver.getTitle();
        Thread.sleep(2000);
        assertEquals(ExpectedOutput ,actualOutput);
    }
    @Test(priority = 2)
    public  void testSignIn(){
        loginPage login = new loginPage(driver);
        boolean log =  login.signIn("vishalbhondu73@gmail.com","Vishal@123");
        if (log){
            System.out.println("login Suceesfull !!");
        }else{
            System.out.println("try again !!");
        }
    }
    @Test(priority = 3)
    public void testsearchBar(){
        searchBar searchbox = new searchBar(driver);
        if (searchbox.search("samsung s22")){
            System.out.println(" got product !!");
        }else {
            System.out.println("try again");
        }
    }
    @Test(priority = 4)
    public void testCart(){
        shoppingCart yourcart = new shoppingCart(driver);
        if (yourcart.cart("samsung s22")){
            System.out.println("added !!");
        }else {
            System.out.println(" out of Stock !!");
        }
    }

    @Test(priority = 5)
    public void logOut(){
        signOut out = new signOut(driver);
        boolean logback = out.logout();
        if (logback){
            System.out.println("login Again !!");
        }else {
            System.out.println("try again !!");
        }

    }



}
